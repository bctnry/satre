let CLI_OPTIONS: { [key: string]: any } = {};

export const getCLIOption = (key: string) => {
    return CLI_OPTIONS[key];
}

export const setCLIOption = (key: string, value: any) => {
    CLI_OPTIONS[key] = value;
}

export const withCLIOptions = (...options: string[]) => (f: (...args: any[]) => any) => (...args: any[]) => {
    options.forEach((v) => setCLIOption(v, (args as any)[args.length - 1][v]));
    return f(...args);
};

export const onCLIOption = (optionName: string, callback: () => any) => {
    if (CLI_OPTIONS[optionName]) {
        callback();
    }
}
