export const CONST = {
    ERROR_CACHE: 'Error reading cache. Will operate as if no cache is present.',
    NO_CACHE: 'Cache file not found. WIll operate as if no cache is present.',
    ERROR_ENCODING: 'Error encoding. Should be in UTF-8.'
}
