export const info = (x: any) => {
    process.stderr.write(`INFO ${x}\n`);
}

export const warn = (x: any) => {
    process.stderr.write(`WARN ${x}\n`)
}

export const error = (x: any) => {
    process.stderr.write(`ERROR ${x}\n`);
}
