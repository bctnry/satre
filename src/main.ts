#!/usr/bin/env node
import { Command } from "commander";
import { withCLIOptions, setCLIOption } from "./CLIOptionService";
import * as CLI from "./CLI";

const main = new Command();

main.version('0.0.1');

main
    .command('run [filename]')
    .description('Build the site starting from `filename`')
    .option('-v, --verbose')
    .option('-C, --no-cache')
    .action(
        withCLIOptions(
            'verbose',
            'cache'
        )(
            CLI.run
        )
    )

main
    .command('runSingle [filename]')
    .description('Build the file without following any relative links')
    .action(
        (filename: string) => {
            setCLIOption('single', true);
            CLI.run(filename);
        }
    )

main.parse(process.argv);
