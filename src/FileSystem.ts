import fs from "fs";
import crypto from "crypto";
import { error } from "./Reporting";
import { CONST } from "./Constant";

let DECODER = new TextDecoder();

export const readFileSync = (path: string) => {
    let _string = '';
    let _buffer = fs.readFileSync(path);
    _string = DECODER.decode(_buffer, {});
    return _string;
}

export const writeFileSync = (path: string, content: string) => {
    fs.writeFileSync(path, content);
}

export const getDigest = (data: string): string => {
    return crypto.createHash('sha256').update(data).digest().toString('base64');
}
