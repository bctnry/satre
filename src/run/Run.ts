import path from "path";
import CommonMark from "commonmark";
import { readFileSync, writeFileSync, getDigest } from "../FileSystem";
import { getCurrentWorkingDir, enterWorkingDir, leaveCurrentWorkingDir } from "../GlobalState";
import { getCLIOption } from "../CLIOptionService";
import { CACHE } from "../CacheService";
import { info } from "../Reporting";

let PARSER = new CommonMark.Parser();
let WRITER = new CommonMark.HtmlRenderer();

let GLOBAL_RUN_STACK: string[] = [];

const _designateResultFilename = (x: string) => {
    return (
        x.endsWith('.md')? x.substring(0, x.length - 3).concat('.html')
        : x
    );
}

const _rmLinkPrefix = (prefix: string) => (x: string) => {
    if (!x.startsWith(prefix)) {
        throw new TypeError(`String ${JSON.stringify(x)} does not have a prefix of ${JSON.stringify(prefix)}`);
    }
    return x.substring(prefix.length);
}

export const runFrom = (startingFile: string, designatedResultFilename?: string) => {
    let _startingPath = path.join(getCurrentWorkingDir(), startingFile);
    // setup current working dir
    // when a Markdown file inside a subdirectory uses link like 'md:./blah.md' it should
    // refer to the sub dir, not the working dir initialized when the run process started,
    // so that all relative path is relative where the document actually inhabits in.
    enterWorkingDir(path.dirname(_startingPath));
    if (!designatedResultFilename) { designatedResultFilename = _designateResultFilename(startingFile); }
    let _source = readFileSync(_startingPath);
    let _parsed = PARSER.parse(_source);
    let _walker = _parsed.walker();
    let _node;
    let _headerFileList: string[] = [];
    let _footerFileList: string[] = [];
    while (_node = _walker.next()) {
        if (_node.node.type === 'link') {
            if (_node.node.destination) {
                if (_node.node.destination.startsWith('md:')) {
                    if (_node.entering) {
                        let _linkedFileName = _rmLinkPrefix('md:')(_node.node.destination);
                        if (!getCLIOption('single') && !GLOBAL_RUN_STACK.includes(_linkedFileName)) {
                            let designatedResultFilename = _designateResultFilename(_linkedFileName);
                            GLOBAL_RUN_STACK.push(_linkedFileName);
                            runFrom(_linkedFileName, designatedResultFilename);
                            GLOBAL_RUN_STACK.pop();
                            _node.node.destination = designatedResultFilename;
                        }
                    }
                } else if (_node.node.destination.startsWith('header:')) {
                    if (_node.entering) {
                        _headerFileList.push(_rmLinkPrefix('header:')(_node.node.destination));
                    } else {
                        _walker.resumeAt(_walker.next().node, true);
                        _node.node.unlink();
                    }
                } else if (_node.node.destination.startsWith('footer:')) {
                    if (_node.entering) {
                        _footerFileList.push(_rmLinkPrefix('footer:')(_node.node.destination));
                    } else {
                        _walker.resumeAt(_walker.next().node, true);
                        _node.node.unlink();
                    }
                }
            }
        }
    }
    
    let _header: string[] = _headerFileList.map((f) => readFileSync(path.join(getCurrentWorkingDir(), f)));
    let _footer: string[] = _footerFileList.map((f) => readFileSync(path.join(getCurrentWorkingDir(), f)));
    let _digest = getDigest(_source);
    if (!getCLIOption('cache') || (!CACHE[_startingPath]) || (CACHE[_startingPath] && (_digest !== CACHE[_startingPath]))) {
        writeFileSync(path.join(getCurrentWorkingDir(), designatedResultFilename), `${_header.join('\n')}\n${WRITER.render(_parsed)}\n${_footer.join('\n')}`);
        info(`Generated file ${designatedResultFilename}`);
        if (getCLIOption('cache')) {
            CACHE[_startingPath] = _digest;
        }
    }
    // leave working dir at the end.
    leaveCurrentWorkingDir();
}
