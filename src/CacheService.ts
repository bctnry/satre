import fs from "fs";
import path from "path";
import { getCurrentWorkingDir } from "./GlobalState";
import { warn } from "./Reporting";
import { CONST } from "./Constant";
import { readFileSync, writeFileSync } from "./FileSystem";

export let CACHE: {[filepath: string]: string} = {};

export const loadCache = () => {
    let _cachePath = path.join(getCurrentWorkingDir(), '.satrecache');
    // if _cachePath does not exist
    //     then throw Error
    let _cacheString = '';
    try {
        _cacheString = readFileSync(_cachePath);
    } catch (e) {
        switch (e) {
            case 'ENOENT': {
                warn(CONST.NO_CACHE);
                break;
            }
            default: {
                warn(`Generic failure with error code ${e.type}. Will operate as if no cache is present.`);
                break;
            }
        }
    }
    if (_cacheString) {
        CACHE = JSON.parse(_cacheString);
    }
}

export const flushCache = () => {
    writeFileSync(path.join(getCurrentWorkingDir(), '.satrecache'), JSON.stringify(CACHE));
}
