import path from "path";
import { info } from "./Reporting";
import { getCurrentWorkingDir, enterWorkingDir } from "./GlobalState";
import { runFrom } from "./run/Run";
import { getCLIOption, onCLIOption } from "./CLIOptionService";
import { flushCache, loadCache } from "./CacheService";

export const run = (x?: string) => {
    let _startingPath = path.resolve(x || '.buildplan');
    // if file _startingPath not exist
    //     then raise Error
    info(`startingPath ${_startingPath}`);
    enterWorkingDir(path.dirname(_startingPath));
    info(`Compiling starting from ${_startingPath}`);
    if (getCLIOption('cache')) { loadCache(); }
    runFrom(path.basename(_startingPath));
    if (getCLIOption('cache')) { flushCache(); }
}
