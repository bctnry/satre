let WORKING_DIR: string[] = [];

export const enterWorkingDir = (x: string) => {
    WORKING_DIR.push(x);
}

export const getCurrentWorkingDir = () => {
    return WORKING_DIR[WORKING_DIR.length - 1];
}

export const leaveCurrentWorkingDir = () => {
    WORKING_DIR.pop();
}
