# Satre

Static site generation with sanity.

Currently only support Markdown (through CommonMark referential implementation).

## Usage

### Static site generation

Any link starting with `md:` will be specially treated as a reference to another Markdown file, causing that file to be converted to HTML. "Circular" linking is supported by traversing the linking graph in a depth-first manner.

Use the command `satre run [filename]` to start the building process from `[filename]`.

If run without providing a starting file, Satre will try to start the process from the `.buildplan` file under the current working directory.

### Headers & Footers

Any link starting with `header:` and `footer:` will be removed from the actual output; the content of the files they linked to will be included as headers & footers, inserted into the output in the appearing order of the links. Normally you would like to put the header & footer links at the beginning of a file or the top-most & bottom-most position.

### Cache

Satre will generate a `.satrecache` file under the working directory which includes digests for all Markdown documents that have been visited in the previous run; only the files that have changed between the previous run & the current run (this will make the files having a different digest) will be re-rendered. To change this behaviour & force Satre to recompile all Markdown file, remove the `.satrecache` file or use the command line flag `--no-cache`.

### Generating for single file

Use the command `satre runSingle [filename]` to convert `[filename]` to HTML without following the `md:` links.

## Installation

From source:

``` bash
git clone [X]/satre.git
cd satre
npm install
npm run build
npm install -g .
# or `npm link`
```

## Usage

```
satre run [markdown filename]
```

## License

MIT
